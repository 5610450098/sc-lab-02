package controller;

import gui.InvestmentFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;

public class CBankAccount {
	private BankAccount account;
	private InvestmentFrame frame;
	private ActionListener list;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	
	private static final double INITIAL_BALANCE = 1000;   
	
	public CBankAccount(){
//	frame = new InvestmentFrame();	
	account = new BankAccount(INITIAL_BALANCE);
	createFrame();
	frame.setResultLabel(account);
		
	}
	
	public static void main (String[] args){
		new CBankAccount();
	}
	public void createFrame(){
		frame = new InvestmentFrame();
		frame.setVisible(true);
//		frame.pack();
	    frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	    list = new AddInterestListener();
	    frame.setListenner(list);
	}
	
	class AddInterestListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			 double rate = Double.parseDouble(frame.getRateField());
	         double interest = account.getBalance() * rate / 100;
	         account.deposit(interest);
	         frame.setResultLabel(account);
//	         resultLabel.setText("balance: " + account.getBalance());
		}
		
	}
	
	
}
